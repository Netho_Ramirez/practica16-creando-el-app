﻿using OpenNETCF.MQTT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using LiteDB;

namespace Practica16
{
    public partial class MainPage : ContentPage
    {
        MQTTClient mqtt;
        List<string> mensajes;
        int m = 0;
        public MainPage()
        {
            InitializeComponent();
            mensajes = new List<string>();
            mqtt = new MQTTClient("broker.hivemq.com", 1883);
            mqtt.MessageReceived += Mqtt_MessageReceived;
            mqtt.Connect("Practica10Netho2");
            mqtt.Subscriptions.Add(new Subscription("ServerNetho2"));
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                if (mensajes.Count > m)
                {
                    lstMensajes.ItemsSource = null;
                    lstMensajes.ItemsSource = mensajes;
                    m = mensajes.Count;
                }
                return true;
            });
        }

        protected override void OnAppearing()
        {
            Title = "Inicio";
        }

        private void Mqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            mensajes.Add(Encoding.UTF8.GetString(payload));
            string msj = Encoding.UTF8.GetString(payload);
            string d = "";
            int v = 0;
            if (msj.Contains("LED"))
            {
                d = "LED";
                v = msj.Contains("Encendido") ? 1 : 0;
            }
            if (msj.Contains("Temperatura"))
            {
                d = "Temperatura";
                v = int.Parse(msj.Substring(14));
            }
            if (msj.Contains("Humedad"))
            {
                d = "Humedad";
                v = int.Parse(msj.Substring(10));
            }
            if (msj.Contains("LDR"))
            {
                d = "LDR";
                v = int.Parse(msj.Substring(6));
            }
            if (msj.Contains("Status") && msj.Contains("APAGADO"))
            {
                d = "Status LED";
                v = 0;
            }
            if (msj.Contains("Status") && msj.Contains("ENCENDIDO"))
            {
                d = "Status LED";
                v = 1;
            }
            if (msj.Contains("Foco"))
            {
                d = "Foco";
                v = msj.Contains("Encendido") ? 1 : 0;
            }
            if (msj.Contains("Status") && msj.Contains("foco") && msj.Contains("APAGADO"))
            {
                d = "Status Foco";
                v = 0;
            }
            if (msj.Contains("Status") && msj.Contains("foco") && msj.Contains("ENCENDIDO"))
            {
                d = "Status Foco";
                v = 1;
            }
            DAL.Insertar(new BD()
            {
                Dispositivo = d,
                Valor = v
            });
        }

        private void btnEncenderLed_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("NethoPractica2", "L1", QoS.FireAndForget, false);
            }
        }

        private void btnApagarLed_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("NethoPractica2", "L0", QoS.FireAndForget, false);
            }
        }

        private void btnEstatusLed_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("NethoPractica2", "L?", QoS.FireAndForget, false);
            }
        }

        private void btnHum_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("NethoPractica2", "H?", QoS.FireAndForget, false);
            }
        }

        private void btnTemp_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("NethoPractica2", "T?", QoS.FireAndForget, false);
            }
        }

        private void btnConsultarLuminosidad_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("NethoPractica2", "R?", QoS.FireAndForget, false);
            }
        }

        private void btnEncenderFoco_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("NethoPractica2", "F1", QoS.FireAndForget, false);
            }
        }

        private void btnApagarFoco_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("NethoPractica2", "F0", QoS.FireAndForget, false);
            }
        }

        private void btnEstatusFoco_Clicked(object sender, EventArgs e)
        {
            if (mqtt.IsConnected)
            {
                mqtt.Publish("NethoPractica2", "F?", QoS.FireAndForget, false);
            }
        }

        private void btnBD_Clicked(object sender, EventArgs e)
        {
            lstBD.ItemsSource = null;
            lstBD.ItemsSource = DAL.Leer();
        }

        private void btnGraficas_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Graficas());
        }
    }
}
