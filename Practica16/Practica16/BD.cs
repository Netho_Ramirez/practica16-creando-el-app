﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica16
{
    public class BD
    {
        public string Id { get; set; }
        public DateTimeOffset FechaHora { get; set; }
        public string Dispositivo { get; set; }
        public int Valor { get; set; }
        public override string ToString()
        {
            return $"{FechaHora} {Dispositivo} => {Valor}";
        }
    }
}
