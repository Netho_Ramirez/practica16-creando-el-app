﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Practica16
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Graficas : ContentPage
    {
        public Graficas()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            Title = "Graficas";
        }


        private void btnGTemp_Clicked_1(object sender, EventArgs e)
        {
            List<BD> datos = DAL.Leer();
            PlotModel grafica = new PlotModel();
            DateTimeAxis ejeTiempo = new DateTimeAxis();
            LineSeries temperatura = new LineSeries();
            foreach (var item in datos)
            {
                temperatura.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora.DateTime, item.Valor));
            }
            temperatura.Title = "Temperatura";
            grafica.Axes.Add(ejeTiempo);
            grafica.Series.Add(temperatura);
            grafico.Model = grafica;
        }

        private void btnGHum_Clicked(object sender, EventArgs e)
        {
            List<BD> datos = DAL.Leer();
            PlotModel grafica = new PlotModel();
            DateTimeAxis ejeTiempo = new DateTimeAxis();
            LineSeries humedad = new LineSeries();
            foreach (var item in datos)
            {
                humedad.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora.DateTime, item.Valor));
            }
            humedad.Title = "Humedad";
            grafica.Axes.Add(ejeTiempo);
            grafica.Series.Add(humedad);
            grafico.Model = grafica;
        }

        private void btnGLum_Clicked(object sender, EventArgs e)
        {
            List<BD> datos = DAL.Leer();
            PlotModel grafica = new PlotModel();
            DateTimeAxis ejeTiempo = new DateTimeAxis();
            LineSeries luminosidad = new LineSeries();
            foreach (var item in datos)
            {
                luminosidad.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora.DateTime, item.Valor));
            }
            luminosidad.Title = "Luminosidad";
            grafica.Axes.Add(ejeTiempo);
            grafica.Series.Add(luminosidad);
            grafico.Model = grafica;
        }
    }
}