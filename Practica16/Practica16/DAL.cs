﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xamarin.Forms;

namespace Practica16
{
    public class DAL
    {
        public static void Insertar(BD registro)
        {
            try
            {
                registro.Id = Guid.NewGuid().ToString();
                registro.FechaHora = DateTime.Now;
                using (var db = new LiteDatabase(new ConnectionString() { Filename = DependencyService.Get<IDataBaseAccess>().DatabasePath() }))
                {
                    db.GetCollection<BD>(typeof(BD).Name).Insert(registro);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static List<BD> Leer()
        {
            try
            {
                List<BD> datos = new List<BD>();
                using (var db = new LiteDatabase(DependencyService.Get<IDataBaseAccess>().DatabasePath()))
                {
                    datos = db.GetCollection<BD>(typeof(BD).Name).FindAll().OrderBy(c => c.FechaHora).ToList();
                }
                return datos;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
